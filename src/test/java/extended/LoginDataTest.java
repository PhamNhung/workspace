package extended;

import Common.BaseTest;
import Util.ExcelUtil;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.loginpageObject;

import java.io.IOException;
public class LoginDataTest extends BaseTest {
    WebDriver driver;
    loginpageObject loginPage;
    @Parameters({ "browser" })
    @BeforeClass
    public void beforeClass(String browserName) {
        String urlPage = "https://workspace.onschool.edu.vn/index.php/signin";
        driver = getBrowserDriver(urlPage, browserName);
        loginPage = new loginpageObject(driver);
    }

    @AfterClass
    public void afterClass() {
        driver.quit();
    }
    @Test
    public void LoginExcel() throws IOException {
        ExcelUtil.excelFilePath("Test auto");
        for(int i=1;i<=ExcelUtil.getRowCountInSheet();i++) {

            String excelUserName = ExcelUtil.getCellData(i, 1);
            String excelPassword = ExcelUtil.getCellData(i,2 );
            System.out.println("Account using: " + excelUserName + "," + excelPassword);
            loginPage.refeshCurrentPage(driver);
            loginPage.inputEmail(excelUserName, "Email");
            loginPage.inputPassword(excelPassword, "Password");
            loginPage.clickButtonLogin();
            //homePage = new HomePageObject(driver);

            Assert.assertTrue(loginPage.checkPageUrl("https://workspace.onschool.edu.vn/index.php/dashboard"));

        }
    }
}