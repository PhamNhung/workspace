package pageObjects;

import Common.BasePage;
import org.openqa.selenium.WebDriver;
import pageUIs.loginpageUI;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
public class loginpageObject extends BasePage {
    WebDriver driver;

    public loginpageObject(WebDriver driver) {
        this.driver = driver;
    }
    public boolean checkPageUrl(String value) {
        String errorMessage = getPageUrl(driver);
        return errorMessage.equals(value);
    }
    public void inputEmail(String value , String textbox) {
        waitForElementvisible(driver, loginpageUI.input_email,textbox);
        sendKeysToElement(driver, loginpageUI.input_email, value,textbox);
    }
    public void inputPassword(String value , String textbox) {
        waitForElementvisible(driver, loginpageUI.input_password,textbox);
        sendKeysToElement(driver, loginpageUI.input_password, value,textbox);
    }
    public void clickButtonLogin() {
        waitForElementvisible(driver, loginpageUI.button_login);
        clickToElement(driver, loginpageUI.button_login);
    }



}
